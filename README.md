oswatcher
=============

#### Table of Contents

1. [Overview](#overview)
2. [Module Description - What the module does and why it is useful](#module-description)
3. [Setup - The basics of getting started with oswatcher](#setup)
    * [What oswatcher affects](#what-oswatcher-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with oswatcher](#beginning-with-oswatcher)
4. [Usage - Configuration options and additional functionality](#usage)
    * [Customize the oswatcher options](#customize-the-oswatcher-options)
    * [Configure with hiera yaml](#configure-with-hiera-yaml)
5. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
    * [Classes](#classes)
6. [Limitations - OS compatibility, etc.](#limitations)
7. [Development - Guide for contributing to the module](#development)
8. [Contributors](#contributors)

## Overview

Puppet Module to Install and set up Oracle oswatcher

## Module Description

The oswatcher module can download and unzip the oswatcher scripts and
install the rpm used to manages the init service on boot.

## Setup

### What oswatcher affects

* download the tar and extract to /opt/oswbb
* download the service rpm and install
* take over management of /etc/oswbb.conf
* start/enable the /etc/init.d/oswbb service

### Setup Requirements

only need to install the module

### Beginning with oswatcher

Minimal oswatcher setup:

```puppet
class { 'oswatcher': }
```

## Usage

### Customize the oswatcher options

```puppet
class { 'oswatcher':
  interval    => '30',
  retention   => '48',
  compression => 'gzip',
}
```

### Configure with hiera yaml

```puppet
include oswatcher
```
```yaml
---
oswatcher::interval: '30'
oswatcher::retention: '48'
oswatcher::compression: 'gzip'
```

## Reference

### Classes

* oswatcher

## Limitations

This module has been built on and tested against Puppet 3.2.4 and higher.  
While I am sure other versions work, I have not tested them.

This module supports modern RedHat only systems.  
This module has been tested on Oracle Linux 6.x and CentOS 7.x.

No plans to support other versions (unless you add it :)..

## Development

Pull Requests welcome

## Contributors

Chris Edester (edestecd)
