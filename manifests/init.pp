# init.pp
# Main class of oswatcher
# Declare main config here
#
# http://docs.oracle.com/cd/E37670_01/E37355/html/ol_oswatcher_diag.html
# http://www.dbaexpert.com/blog/the-better-way-to-configure-oswatcher/
#

class oswatcher (
  $tmp_path       = $oswatcher::params::tmp_path,
  $install_root   = $oswatcher::params::install_root,
  $archive        = $oswatcher::params::archive,
  $compression    = $oswatcher::params::compression,
  $interval       = $oswatcher::params::interval,
  $retention      = $oswatcher::params::retention,
  $service_ensure = $oswatcher::params::service_ensure,
  $service_enable = $oswatcher::params::service_enable,
) inherits oswatcher::params {

  validate_absolute_path($tmp_path)
  validate_absolute_path($install_root)

  anchor { 'oswatcher::begin': }
  -> class { 'oswatcher::install': }
  -> class { 'oswatcher::config': }
  ~> class { 'oswatcher::service': }
  -> anchor { 'oswatcher::end': }

  Class['oswatcher::install'] ~> Class['oswatcher::service']

}
