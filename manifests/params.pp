# params.pp
# Set up parameters defaults etc.
#

class oswatcher::params {

  if ($::osfamily == 'RedHat') and (versioncmp($::operatingsystemrelease, '5.0') >= 0) {
    # install
    $tmp_path              = '/opt/oracle/oradata2/oswbb'
    $install_root          = '/opt'
    $oswbb_version         = '733'
    $oswbb_service_version = '7.2.0-1'

    # config

    # Note: if you update the version of the tar install file
    # I had to Change:
    # startOSWbb.sh:40 to:
    # pgrep OSWatcher > /dev/null
    # in our files/oswbb733.tar
    # Or else setting OSW_ARCHIVE to a dir on a nfs mount
    # would fail to start the service, due to race condition

    $archive     = "/opt/oracle/oradata2/OSWatcher_archive/${::hostname}" # 'archive'
    $compression = 'gzip'
    $interval    = '30'
    $retention   = '48'

    # service
    $service_ensure = running
    $service_enable = true
  } else {
    fail("The ${module_name} module is not supported on a ${::osfamily} based system with version ${::operatingsystemrelease}.")
  }

}
