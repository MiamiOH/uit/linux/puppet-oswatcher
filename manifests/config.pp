# config.pp
# Manage oswbb.conf config file
#

class oswatcher::config inherits oswatcher {

  file { '/etc/oswbb.conf':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template("${module_name}/oswbb.conf.erb"),
  }

}
