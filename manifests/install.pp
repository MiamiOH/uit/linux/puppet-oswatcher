# install.pp
# Download to tmp dir and install
#

class oswatcher::install {

  $tar_file = "oswbb${oswatcher::oswbb_version}.tar"
  $rpm_file = "oswbb-service-${oswatcher::oswbb_service_version}.noarch.rpm"

  File {
    ensure => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
  }

  # I had to Change:
  # startOSWbb.sh:40 to:
  # pgrep OSWatcher > /dev/null
  # in our files/oswbb733.tar
  # Or else setting OSW_ARCHIVE to a dir on a nfs mount
  # would fail to start the service, due to race condition

  # lint:ignore:source_without_rights
  file { "${oswatcher::tmp_path}/${tar_file}":
    source => "puppet:///modules/${module_name}/${tar_file}",
  }

  -> file { "${oswatcher::tmp_path}/${rpm_file}":
    source => "puppet:///modules/${module_name}/${rpm_file}",
  }
  # lint:endignore

  -> exec { 'remove-old-oswbb':
    command => "rm -rf ${oswatcher::install_root}/oswbb",
    creates => "${oswatcher::install_root}/oswbb/version-${oswatcher::oswbb_version}",
    path    => '/usr/bin:/bin',
  }

  -> exec { 'extract-new-oswbb':
    command => "tar -xf ${oswatcher::tmp_path}/${tar_file} -C ${oswatcher::install_root} && \
                touch ${oswatcher::install_root}/oswbb/version-${oswatcher::oswbb_version}",
    creates => "${oswatcher::install_root}/oswbb",
    path    => '/usr/bin:/bin',
  }

  -> package { 'oswbb-service':
    ensure   => $oswatcher::oswbb_service_version,
    provider => rpm,
    source   => "${oswatcher::tmp_path}/${rpm_file}",
  }

}
