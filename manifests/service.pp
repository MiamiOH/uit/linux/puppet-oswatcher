# service.pp
# Manage service
#

class oswatcher::service {

  service { 'oswbb':
    ensure     => $oswatcher::service_ensure,
    enable     => $oswatcher::service_enable,
    hasrestart => true,
    hasstatus  => true,
  }

}
