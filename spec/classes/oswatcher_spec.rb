require 'spec_helper'

describe 'oswatcher' do
  on_supported_os.each do |os, facts|
    context "on #{os}" do
      let(:facts) do
        facts.merge(environment: 'test', concat_basedir: '/var/lib/puppet/concat')
      end

      context 'with defaults' do
        it { is_expected.to compile.with_all_deps }
        it { is_expected.to contain_class('oswatcher::install') }
        it { is_expected.to contain_class('oswatcher::config') }
        it { is_expected.to contain_class('oswatcher::service') }
      end
    end
  end
end
